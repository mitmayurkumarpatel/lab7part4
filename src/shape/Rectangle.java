package shape;
/**
 *
 * @author mitpatel
 */
public class Rectangle extends Shape
{
    double length,width;
    
    public Rectangle(String colour, double length, double width)
    {
        super(colour);
        this.length = length;
        this.width = width;
    }    
    double area()
    {
        return length * width;
    }
    public String toString()
    {
         return "Rectangle is = "+ super.getColour() +" and area of Rectangle is : " +area();
    }
    
}
