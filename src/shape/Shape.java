package shape;
/**
 *
 * @author mitpatel
 */
public abstract class Shape
{
    private String colour;
    
    //abstract method
    abstract double area();
    public abstract String toString();
    
    public Shape(String colour)
    {
        this.colour = colour;  
    }

    public String getColour() {
        return colour;
    } 
}
