package shape;

/**
 *
 * @author mitpatel
 */
 class Circle extends Shape
{
     //default variable
    double radius;
    
    public Circle(String colour, double radius)
    {
        super(colour);//called colour from superclass
        this.radius = radius;
        
    }
    double area()
    {
        return Math.PI * Math.pow(radius, 2);
    }
    public String toString()
    {
        return "Circle is = "+ super.getColour() +" and area of circle is : " +area();
    }
    
    
}
