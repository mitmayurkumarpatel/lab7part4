package shape;

/**
 *
 * @author mitpatel
 */
public class ShapeSimulation 
{
    public static void main(String[]args)
    {
        Shape s1 = new Circle("red",8);
        Shape s2 = new Rectangle("yellow",6,6);
        
        System.out.println(s1.toString());
        System.out.println(s2.toString());
        
        
    }
    
}
